// test_igm_3.cpp
//
// Testing the Jacobian of the CoM. To include an igm function based on it.
//
// [2011/11/26]

#include <iostream>
#include <stdio.h>
#include <math.h>

#include <sys/time.h>
#include <time.h>

#include "zeno_igm.h"
#include "util.cpp"
#include "maple_functions.h"

using namespace std;


int main(int argc, char** argv)
{
    zeno_igm zeno;

    struct timeval start, end;
    double cTime;
    int test_N = 1000;


    // set initial configuration
    jointState q0;

    zeno.init (
            IGM_SUPPORT_LEFT,
            0.0, 0.044810, 0.0,
            0.0, 0.0, 0.0);
    q0 = zeno.state_model;

    zeno.getSwingFootPosture (zeno.state_sensor, zeno.right_foot_posture.data());

    MatrixPrint(4,4,zeno.right_foot_posture.data(),"right_foot_posture2");

    zeno.getCoM (zeno.state_sensor, zeno.CoM_position);
    printf(" com = (%f, %f, %f) \n", zeno.CoM_position[0], zeno.CoM_position[1] , zeno.CoM_position[2]);
    
    zeno.right_foot_posture = 
        (zeno.right_foot_posture) *
        Translation<double,3>(0.02,0.0,0.01) *
        AngleAxisd(0.0, Vector3d::UnitX()) *
        AngleAxisd(0.0, Vector3d::UnitY()) *
        AngleAxisd(0.0, Vector3d::UnitZ());

    MatrixPrint(4,4,zeno.right_foot_posture.data(),"right_foot_posture3");

    zeno.getCoM (zeno.state_sensor, zeno.CoM_position);

    zeno.CoM_position[0] += 0.03;
    zeno.CoM_position[1] += 0.02;
    //zeno.CoM_position[2] -= 0.01;
    printf(" com = (%f, %f, %f) \n", zeno.CoM_position[0], zeno.CoM_position[1] , zeno.CoM_position[2]);
    
    int iter;
    gettimeofday(&start,0);
    for (int i=0 ; i<test_N ; i++)
    {
        zeno.state_model = q0;
        iter = zeno.igm(q0.q, 1.2, 0.0015, 20);
    }
    gettimeofday(&end,0);
    cTime = end.tv_sec - start.tv_sec + 0.000001 * (end.tv_usec - start.tv_usec);
    printf(" time (igm) = % f\n", cTime/test_N);

    cout << "iter = " << iter << endl;

    MatrixPrint(1,12,zeno.state_model.q,"q");

    int check_bounds = zeno.state_model.checkJointBounds();
    if (check_bounds >= 0)
    {
        cout << "Bounds are violated! ID:" << check_bounds << endl;
    }
    else
    {
        cout << "Bounds are ok." << endl;
    }

    return 0;
}

