/**
 * @file
 * @author Alexander Sherikov
 * @date 02.12.2011 15:18:26 MSK
 */


#ifndef MAPLE_FUNCTIONS_H
#define MAPLE_FUNCTIONS_H


/****************************************
 * PROTOTYPES 
 ****************************************/

extern "C" {
    void LLeg2RLeg(double *q,double *LL,double *T);
    void LLeg2Waist(double *q,double *LL,double *T);
    void LLeg2WaistCoM(double *q,double *LL,double *A);
    void LLeg2CoM(double *q,double *LL,double *A);
    void RLeg2LLeg(double *q,double *RL,double *T);
    void RLeg2Waist(double *q,double *RL,double *T);
    void RLeg2WaistCoM(double *q,double *RL,double *A);
    void RLeg2CoM(double *q,double *RL,double *A);
    void Waist2LLeg(double *q,double *LL,double *T);
    void Waist2RLeg(double *q,double *LL,double *T);
    void Waist2Torso(double *q,double *LL,double *T);
    void Waist2Head(double *q,double *LL,double *T);
    void Waist2LHand(double *q,double *LL,double *T);
    void Waist2RHand(double *q,double *LL,double *T);
    void Waist2CoM(double *q,double *LL,double *T);
    void LLeg2Joints(double *q,double *LL,double *A);
    void RLeg2Joints(double *q,double *RL,double *A);

    void Euler2T(double x,double y,double z,double X,double Y,double Z,double *T);
    void Euler2Rot(double X,double Y,double Z,double *R);

    /**
     * IK with waist roll and pitch rotation
     */
    void from_LLeg_3(double *q,double *LL, double *RL,double *CoM,double *Rot, double *A);
    void from_RLeg_3(double *q,double *RL, double *LL,double *CoM,double *Rot, double *A);

    /**
     * IK with waist pitch rotation
     */
    void from_LLeg_3_new(double *q,double *LL,double *RL,double *CoM,double *Rot,double *A);
    void from_RLeg_3_new(double *q,double *RL,double *LL,double *CoM,double *Rot,double *A);

    /**
     * IK with no waist rotation
     */
    void from_LLeg_3_no_Waist_rotation(double *q,double *LL,double *RL,double *CoM,double *A);
    void from_RLeg_3_no_Waist_rotation(double *q,double *RL,double *LL,double *CoM,double *A);

    /**
     * IK taken waist as base link
     */
    void from_LLeg_3_waist(double *q,double *LL,double *RL,double *CoM,double *A);
    void from_RLeg_3_waist(double *q,double *RL,double *LL,double *CoM,double *A);
}

#endif //MAPLE_FUNCTIONS_H

