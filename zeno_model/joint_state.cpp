/**
 * @file
 * @author Alexander Sherikov
 */


#include "zeno_igm.h"
#include "maple_functions.h"


double jointState::q_lower_bound[JOINTS_NUM];
double jointState::q_upper_bound[JOINTS_NUM];



/**
 * @brief Constructor
 */
jointState::jointState()
{
    initBounds();
    initJointAngles();
}



/**
 * @brief Check that all joint angles lie within bounds.
 *
 * @return -1 if all values are corrent, id of the first joint violating the
 * bounds.
 *
 * @attention No collision checks!
 */
int jointState::checkJointBounds()
{
    for (int i = 0; i < JOINTS_NUM; i++)
    {
        if ((q_lower_bound[i] > q[i]) || (q_upper_bound[i] < q[i]))
        {
            std::cout << "violated joint ( joint id , value) : " << "(" << i << "," << q[i] << ")" <<std::endl;
            return (i);
        }
    }
    return (-1);
}


/** @brief Sets the initial configuration of zeno (lets call it the standard initial configuration)
    \note Only q[0]...q[23] are set. The posture of the base is not set.
*/
void jointState::initJointAngles()
{
    q_init[L_HIP_ROLL]       =       0.0;
    q_init[L_HIP_YAW]        =       0.0;
    q_init[L_HIP_PITCH]      =       -0.5;//-0.35; //-0.436332;
    q_init[L_KNEE_PITCH]     =       0.5;//0.75;//0.436332;
    q_init[L_ANKLE_PITCH]    =       -0.1;//-0.416; //-0.436332/4;
    q_init[L_ANKLE_ROLL]     =       0.0;

    q_init[R_HIP_ROLL]       =       0.0;
    q_init[R_HIP_YAW]        =       0.0;
    q_init[R_HIP_PITCH]      =       -0.5;//-0.35; //-0.436332;
    q_init[R_KNEE_PITCH]     =       0.5;//0.75; //0.436332;
    q_init[R_ANKLE_PITCH]    =       -0.1;//-0.416; //-0.436332/4;
    q_init[R_ANKLE_ROLL]     =       0.0;

    q_init[L_SHOULDER_PITCH] =       1.0;//0.78539;
    q_init[L_SHOULDER_ROLL]  =       0.25;//0.78539;
    q_init[L_ELBOW_ROLL]     =       -0.1;
    q_init[L_ELBOW_YAW]      =       -0.1;//-0.78539;
    q_init[L_WRIST_YAW]      =       0.0;

    q_init[R_SHOULDER_PITCH] =       1.0;//0.78539;
    q_init[R_SHOULDER_ROLL]  =       -0.25;//-0.78539;
    q_init[R_ELBOW_ROLL]     =       0.1;
    q_init[R_ELBOW_YAW]      =       0.1;//0.78539;
    q_init[R_WRIST_YAW]      =       0.0;

    q_init[HEAD_PITCH]       =       0.0;
    q_init[HEAD_YAW]         =       0.0;
    q_init[HEAD_ROLL]        =       0.0;

    q_init[TORSO_YAW]        =       0.0;

    
    q[L_HIP_ROLL]       =       0.0;
    q[L_HIP_YAW]        =       0.0;
    q[L_HIP_PITCH]      =       -0.35; //-0.436332;
    q[L_KNEE_PITCH]     =       0.75; //0.436332;
    q[L_ANKLE_PITCH]    =       -0.416; //-0.436332/4;
    q[L_ANKLE_ROLL]     =       0.0;

    q[R_HIP_ROLL]       =       0.0;
    q[R_HIP_YAW]        =       0.0;
    q[R_HIP_PITCH]      =       -0.35; //-0.436332;
    q[R_KNEE_PITCH]     =       0.75; //0.436332;
    q[R_ANKLE_PITCH]    =       -0.416; //-0.436332/4;
    q[R_ANKLE_ROLL]     =       0.0;

    q[L_SHOULDER_PITCH] =       1.0;//0.78539;
    q[L_SHOULDER_ROLL]  =       0.25;//0.78539;
    q[L_ELBOW_ROLL]     =       -0.1;
    q[L_ELBOW_YAW]      =       -0.1;//-0.78539;
    q[L_WRIST_YAW]      =       0.0;

    q[R_SHOULDER_PITCH] =       1.0;//0.78539;
    q[R_SHOULDER_ROLL]  =       -0.25;//-0.78539;
    q[R_ELBOW_ROLL]     =       0.1;
    q[R_ELBOW_YAW]      =       0.1;//0.78539;
    q[R_WRIST_YAW]      =       0.0;

    q[HEAD_PITCH]       =       0.0;
    q[HEAD_YAW]         =       0.0;
    q[HEAD_ROLL]        =       0.0;

    q[TORSO_YAW]        =       0.0;

    int check_bound;
    if((check_bound=checkJointBounds()) >= 0)
        std::cout << "Initial configuration violate bounds. ID!" << check_bound << std::endl;
}

/**
 * @brief Initialize bounds.
 */
void jointState::initBounds()
{
    // LEFT LEG
    setBounds(L_HIP_ROLL       ,   -0.7854,    0.7854); //-0.4663,    0.6974); // -0.7854,    0.7854
    setBounds(L_HIP_YAW        ,   -0.7854,    1.5708);
    setBounds(L_HIP_PITCH      ,   -1.4868,       0.0);
    setBounds(L_KNEE_PITCH     ,       0.0,    1.5708);
    setBounds(L_ANKLE_PITCH    ,   -0.7854,    1.3100);
    setBounds(L_ANKLE_ROLL     ,   -0.4363,    0.4363);
                              
    // RIGHT LEG
    setBounds(R_HIP_ROLL       ,   -0.7854,    0.7854); //-0.6974,    0.4663);
    setBounds(R_HIP_YAW        ,   -1.5708,    0.7854);
    setBounds(R_HIP_PITCH      ,   -1.4868,       0.0);
    setBounds(R_KNEE_PITCH     ,       0.0,    1.5708);
    setBounds(R_ANKLE_PITCH    ,   -0.7854,    1.3100);
    setBounds(R_ANKLE_ROLL     ,   -0.4363,    0.4363);
                              
    // LEFT ARM
    setBounds(L_SHOULDER_PITCH ,   -1.5708,    2.0);
    setBounds(L_SHOULDER_ROLL  ,       0.0,    1.6225);
    setBounds(L_ELBOW_ROLL     ,   -3.1416,       0.0);
    setBounds(L_ELBOW_YAW      ,   -1.4908,       -0.08);
    setBounds(L_WRIST_YAW      ,       0.0,    1.5708);
                              
    // RIGHT ARM
    setBounds(R_SHOULDER_PITCH ,   -1.5708,    2.0);
    setBounds(R_SHOULDER_ROLL  ,   -1.6225,       0.0);
    setBounds(R_ELBOW_ROLL     ,         0,    3.1416);
    setBounds(R_ELBOW_YAW      ,       0.08,    1.4908);
    setBounds(R_WRIST_YAW      ,       0.0,    1.5708);

    // HEAD
    setBounds(HEAD_YAW         ,   -1.5708,    1.5708);
    setBounds(HEAD_PITCH       ,         0,    0.5235);
    setBounds(HEAD_ROLL        ,   -0.5235,    0.5235);

    // TORSO
    setBounds(TORSO_YAW        ,   -1.0472,    1.0472);
}


/**
 * @brief Set bounds for a joint.
 *
 * @param[in] id id of the joint.
 * @param[in] lower_bound lower bound.
 * @param[in] upper_bound upper_bound.
 */
void jointState::setBounds (const jointSensorIDs id, const double lower_bound, const double upper_bound)
{
    q_lower_bound[id] = lower_bound;
    q_upper_bound[id] = upper_bound;
}
