/**
 * @file
 * @author Alexander Sherikov
 * @date 02.12.2011 15:17:52 MSK
 */


#ifndef ZENO_IGM_H
#define ZENO_IGM_H

/****************************************
 * INCLUDES 
 ****************************************/

#define EIGEN_DONT_VECTORIZE
#define EIGEN_DISABLE_UNALIGNED_ARRAY_ASSERT
#define Eigen2Support
#define EIGEN2_SUPPORT_STAGE10_FULL_EIGEN2_API
#include <Eigen/Geometry>
#include "joints_sensors_id.h"
#include "maple_functions.h"



/****************************************
 * TYPEDEFS 
 ****************************************/

using namespace Eigen;


enum igmSupportFoot {
    IGM_SUPPORT_RIGHT = 0,
    IGM_SUPPORT_LEFT = 1
};


#define POSITION_VECTOR_SIZE 3



class jointState
{
    public:
        jointState();

        void initJointAngles();
        int checkJointBounds();

        double q[JOINTS_NUM];
        double q_init[JOINTS_NUM];


    private:
        void initBounds();
        void setBounds (const jointSensorIDs, const double, const double);

        static double q_lower_bound[JOINTS_NUM];
        static double q_upper_bound[JOINTS_NUM];
};



class zeno_igm 
{
    public:
        void init (
                const igmSupportFoot,
                const double, const double, const double,
                const double, const double, const double);

        void switchSupportFoot ();
        void setCoM (const double, const double, const double);

        void getFeetPositions (double *, double *, double *, double *);

        void getCoM (jointState&, double *);
        void getWaistCoM(jointState&, double *);
        void getSwingFootPosture (jointState&, double *);
        void getWaistPosture(jointState& joints, double *swing_foot_posture);

        int igm (const double*, const double, const double, const int);
        int igmWaist(const double*, const double, const double, const int);
        int igmWithWaistPitchConstraint( const double *ref_angles, const double mu,
                                        const double tol,
                                        const int max_iter,
                                        const double constraint_pitch = 0.08,
                                        const int no_of_samples = 6,
                                        const double pitch_sampling_step = 0.01);
        int igmWithWaistRollPitchConstraint(
                                        const double *ref_angles,
                                        //const double *rotation,
                                        const double mu,
                                        const double tol,
                                        const int max_iter);

        igmSupportFoot support_foot;

        jointState state_model;
        jointState state_sensor;

        Transform<double,3> left_foot_posture;
        Transform<double,3> right_foot_posture;

        double CoM_position[POSITION_VECTOR_SIZE];
};

#endif // ZENO_IGM_H
